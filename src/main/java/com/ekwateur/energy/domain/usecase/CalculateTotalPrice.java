package com.ekwateur.energy.domain.usecase;

import com.ekwateur.energy.domain.model.Client;

import java.math.BigDecimal;

public abstract class CalculateTotalPrice {

    private CalculateTotalPrice(){}

    public static BigDecimal handle(Client client, Double electricityConsumption, Double gasConsumption) {

        return client.getTotalPrice(electricityConsumption,gasConsumption);

    }

}
