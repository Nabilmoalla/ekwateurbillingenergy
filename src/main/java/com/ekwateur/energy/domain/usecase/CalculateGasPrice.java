package com.ekwateur.energy.domain.usecase;

import com.ekwateur.energy.domain.model.Client;

import java.math.BigDecimal;

public abstract class CalculateGasPrice {

    private CalculateGasPrice(){}

    public static BigDecimal handle(Client client, Double consumption) {

        return client.getGasPrice(consumption);

    }

}
