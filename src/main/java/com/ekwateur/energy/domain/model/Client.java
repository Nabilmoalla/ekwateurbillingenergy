package com.ekwateur.energy.domain.model;


import java.math.BigDecimal;
import java.math.RoundingMode;

public abstract class Client {
    protected String referenceClient;

    protected Client(String referenceClient) {
        this.referenceClient = referenceClient;
    }

    public abstract BigDecimal getElectricityPrice(Double consumption);

    public abstract BigDecimal getGasPrice(Double consumption);

    public BigDecimal getTotalPrice(Double electricityConsumption, Double gasConsumption){
        return getElectricityPrice(electricityConsumption).add(getGasPrice(gasConsumption)).setScale(2, RoundingMode.HALF_UP);
    }
}

