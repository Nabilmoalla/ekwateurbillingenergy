package com.ekwateur.energy.domain.model;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class ProfessionalClient extends Client {

    private static final BigDecimal ELECTRICITY_PRICE_PER_KWH_HIGH_TURNOVER = BigDecimal.valueOf(0.114);
    private static final BigDecimal ELECTRICITY_PER_KWH_LOW_CA = BigDecimal.valueOf(0.118);
    private static final BigDecimal GAS_PRICE_PER_KWH_HIGH_CA = BigDecimal.valueOf(0.111);
    private static final BigDecimal GAS_PRICE_PER_KWH_LOW_CA = BigDecimal.valueOf(0.113);
    private static final BigDecimal TURNOVER_THRESHOLD = BigDecimal.valueOf(1000000);

    private String siret;
    private String companyName;
    private BigDecimal turnover;


    public ProfessionalClient(String referenceClient, String siret, String companyName, BigDecimal turnover) {
        super(referenceClient);
        this.siret = siret;
        this.companyName = companyName;
        this.turnover = turnover;
    }

    @Override
    public BigDecimal getElectricityPrice(Double consumption) {
        return BigDecimal.valueOf(consumption).multiply(turnover.compareTo(TURNOVER_THRESHOLD) > 0 ? ELECTRICITY_PRICE_PER_KWH_HIGH_TURNOVER : ELECTRICITY_PER_KWH_LOW_CA).setScale(2, RoundingMode.HALF_UP);
    }

    @Override
    public BigDecimal getGasPrice(Double consumption) {
        return BigDecimal.valueOf(consumption).multiply(turnover.compareTo(TURNOVER_THRESHOLD) > 0 ? GAS_PRICE_PER_KWH_HIGH_CA : GAS_PRICE_PER_KWH_LOW_CA).setScale(2, RoundingMode.HALF_UP);
    }

}


