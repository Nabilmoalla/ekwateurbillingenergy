package com.ekwateur.energy.domain.model;


import java.math.BigDecimal;
import java.math.RoundingMode;

public class ParticularClient extends Client {

    private static final BigDecimal ELECTRICITY_KW_PRICE = BigDecimal.valueOf(0.121);

    private static final BigDecimal GAS_KW_PRICE = BigDecimal.valueOf(0.115);

    private String civility;
    private String firstName;
    private String lastName;

    public ParticularClient(String referenceClient, String civility, String firstName, String lastName) {
        super(referenceClient);
        this.civility = civility;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    @Override
    public BigDecimal getElectricityPrice(Double consumption) {
        return BigDecimal.valueOf(consumption).multiply(ELECTRICITY_KW_PRICE).setScale(2, RoundingMode.HALF_UP);
    }

    @Override
    public BigDecimal getGasPrice(Double consumption) {
        return BigDecimal.valueOf(consumption).multiply(GAS_KW_PRICE).setScale(2, RoundingMode.HALF_UP);
    }
}
