package com.ekwateur.energy.domain.usecase;

import com.ekwateur.energy.domain.model.Client;
import com.ekwateur.energy.domain.model.ParticularClient;
import com.ekwateur.energy.domain.model.ProfessionalClient;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class CalculateGasPriceTest {


    private static final String REFERENCE_CLIENT = "EKW00000001";

    private static final String SIRET_NUMBER = "12345678925486";

    @Nested
    class ParticularClientTest {

        @Test
        void assertThatPriceIsCorrect() {
            Client particularClient = new ParticularClient(REFERENCE_CLIENT, "Monsieur", "Nabil", "MOALLA");
            Assertions.assertEquals(BigDecimal.valueOf(23.00).setScale(2, RoundingMode.HALF_UP), CalculateGasPrice.handle(particularClient, 200d));
        }


    }

    @Nested
    class ProfessionalClientTest {

        @Test
        void assertThatPriceIsCorrectWhenTurnoverIsGreaterThanOneMillion() {
            Client professionalClient = new ProfessionalClient(REFERENCE_CLIENT, SIRET_NUMBER, "EKWATEUR", BigDecimal.valueOf(2000000));
            Assertions.assertEquals(BigDecimal.valueOf(27.75), CalculateGasPrice.handle(professionalClient, 250d));
        }


        @Test
        void assertThatPriceIsCorrectWhenTurnoverIsLessThanOneMillion() {
            Client professionalClient = new ProfessionalClient(REFERENCE_CLIENT, SIRET_NUMBER, "EKWATEUR", BigDecimal.valueOf(900000));
            Assertions.assertEquals(BigDecimal.valueOf(21.47), CalculateGasPrice.handle(professionalClient, 190d));
        }


    }


}
