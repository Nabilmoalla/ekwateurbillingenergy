package com.ekwateur.energy.domain.usecase;

import com.ekwateur.energy.domain.model.Client;
import com.ekwateur.energy.domain.model.ParticularClient;
import com.ekwateur.energy.domain.model.ProfessionalClient;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

public class CalculateElectricityPriceTest {


    private static final String REFERENCE_CLIENT = "EKW00000001";

    private static final String SIRET_NUMBER = "12345678925486";

    @Nested
    class ParticularClientTest {

        @Test
        void assertThatPriceIsCorrect() {
            Client particularClient = new ParticularClient(REFERENCE_CLIENT, "Monsieur", "Nabil", "MOALLA");
            Assertions.assertEquals(BigDecimal.valueOf(2.42), CalculateElectricityPrice.handle(particularClient, 20d));
        }


    }

    @Nested
    class ProfessionalClientTest {

        @Test
        void assertThatPriceIsCorrectWhenTurnoverIsGreaterThanOneMillion() {
            Client professionalClient = new ProfessionalClient(REFERENCE_CLIENT, SIRET_NUMBER, "EKWATEUR", BigDecimal.valueOf(2000000));
            Assertions.assertEquals(BigDecimal.valueOf(19.38), CalculateElectricityPrice.handle(professionalClient, 170d));
        }


        @Test
        void assertThatPriceIsCorrectWhenTurnoverIsLessThanOneMillion() {
            Client professionalClient = new ProfessionalClient(REFERENCE_CLIENT, SIRET_NUMBER, "EKWATEUR", BigDecimal.valueOf(900000));
            Assertions.assertEquals(BigDecimal.valueOf(14.16), CalculateElectricityPrice.handle(professionalClient, 120d));
        }


    }


}
